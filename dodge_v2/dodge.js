var C = {
  "game": {
    "width": ,
    "height": 568
  },
  "bg": {
    "width": 320,
    "height": 568,
    "xspeed": 0,
    "yspeed": 700,
    "file": "assets/background.png"
  }
"p": {
    "file": "assets/player.png",
    "width": 640,
    "height": 480,
    "frames": 2,
    "startx": 160,
    "starty": 500
  }
}


class Boot {
  preload() {
    this.scale.scalemode = Phaser.ScaleManager.SHOW_ALL;
    this.scale.pageAlignHorizontally = true;
    this.scale.pageAlignVertically = true;
  }

create() {
  this.state.start("Load")
  }

}

class Load {
  preload() {
    console.log("the game will shortly be loaded.");
    this.load.image("bg",C.bg.file)
    this.load.spritesheet("player",C.p.file,C.p.width,C.p.height,C.p.frames);
  }
  create() {
    console.log("Loaded");
    this.state.start("Play")
  }

}

class Play {
  create() {
    console.log("Entered Play State");
        this.background = this.add.tileSprite(0,0,C.game.width,C.game.height);
            this.background.autoScroll(-400,0);
    this.player = this.add.sprite(C.p.startx,C.p.starty,"player");
    this.player.anchor.set(0.5,0.5);
    this.player.smoothed = false;
    this.player.scale.set(1);

  }
}

function restart() {
  game.state.start("Boot");
}

var game = new Phaser.Game(640,480);
game.state.add("Boot",Boot);
game.state.add("Load",Load);
game.state.add("Play",Play);
game.state.start("Boot");

